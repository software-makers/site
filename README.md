# Software Makers

A not-for-profit organization for people making software around the world, 
based in Heraklion, Crete, Greece.

## What

- In legal terms, “Σύλλογος / Σωματείο”
- A non-profit organization
- Run by VOLUNTEERS
- Based in Crete, members can be anywhere!

## Why

- Originally conceived as the official legal entity to run SoCraTes-Crete.org 
- Non-profit event -> non-profit entity to host it
- **Money:** we don’t want individual people to manage funds in our own bank 
  accounts
- **Risk:** we don’t want individual people to take on the risk of running this 
  event  
- **Intellectual Property** 
  - all intellectual property should belong to / be controlled by the 
    community: domains, discord, data, etc.  

## How 

- “Members” elect “executive committee” every 2 years
- “Executive committee” manages day-to-day operations
- “Executive committee” appoints “Org Teams” to manage individual events
  - e.g. SoCraTes-Crete.org OrgTeam !== Software Makers Executive Committee

(For more details, check the [founding document](docs/Software_Makers_Founding_Document.pdf)).

## What’s in it for me ?

As a member, you : 
- pay a 20 eur yearly fee 💸
  - become an official member! 
- agree to respect the Code of Conduct
- get a vote! i.e. you’re in control! 
- are now part of a group of other software makers! 
  - amplified voice,
  - collaborative action, 
  - (and more benefits to come, over time)

## Open Governance

All Executive Committee (EC) meetings are open for anyone to attend. We also 
keep records of all meetings [here](./records).